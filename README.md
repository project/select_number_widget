# Select number widget

Select widget for number fields.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/select_number_widget).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/select_number_widget).


## Requirements

This module requires no modules outside of Drupal core (Node and Block).


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

- Enable the module at Administration > Extend.
- Select the "Select number widget" in your field form display configuration.

## Similar modules

- None found.


## Maintainers

- Ryan Szrama - [rszrama](https://www.drupal.org/u/rszrama)
- Tavi Toporjinschi - [vasike](https://www.drupal.org/u/vasike)

## Original author

- Frank Mably - [mably](https://www.drupal.org/u/mably)
