<?php

namespace Drupal\select_number_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'select number' widget.
 *
 * @FieldWidget(
 *   id = "select_number_widget",
 *   label = @Translation("Select number widget"),
 *   field_types = {
 *     "integer"
 *   }
 * )
 */
class SelectNumberWidget extends NumberWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state,
  ) {
    $value = $items[$delta]->value ?? 0;
    $field_settings = $this->getFieldSettings();

    if (is_numeric($field_settings['min'])
      && is_numeric($field_settings['max'])) {
      // Set minimum and maximum.
      $min = $field_settings['min'];
      $max = $field_settings['max'];

      // Set the step for floating point and decimal numbers.
      switch ($this->fieldDefinition->getType()) {
        case 'decimal':
          $step = pow(0.1, $field_settings['scale']);
          break;

        default:
          $step = 1;
          break;
      }

      // Add prefix and suffix.
      if ($field_settings['prefix']) {
        $prefixes = explode('|', $field_settings['prefix']);
        $prefix = Html::decodeEntities(strip_tags(array_pop($prefixes)));
      }
      else {
        $prefix = NULL;
      }
      if ($field_settings['suffix']) {
        $suffixes = explode('|', $field_settings['suffix']);
        $suffix = Html::decodeEntities(strip_tags(array_pop($suffixes)));
      }
      else {
        $suffix = NULL;
      }

      $element += [
        '#type' => 'select',
        '#options' => SelectNumberWidget::selectOptions(
          $min, $max, $step, $prefix, $suffix),
        '#default_value' => $value,
      ];

      return ['value' => $element];
    }
    else {
      return parent::formElement($items, $delta, $element, $form, $form_state);
    }
  }

  /**
   * Get select number widget options.
   */
  public static function selectOptions($min, $max, $step, $prefix, $suffix): array {
    $options = [];

    $pfx = $prefix ?? '';
    $sfx = $suffix ?? '';

    for ($i = $min; $i <= $max; $i += $step) {
      $options[$i] = $pfx . $i . $sfx;
    }

    return $options;
  }

}
